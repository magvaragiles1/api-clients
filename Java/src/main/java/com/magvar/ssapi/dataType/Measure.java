package com.magvar.ssapi.dataType;

/**
 * This class holds measure which is combination of numeric value and the units
 * the value is in.
 */
public class Measure {

    /**
     * Numerical value
     */
    private double value;
    /**
     * Units of the measure
     */
    private String unit;

    /**
     * Empty constructor
     */
    public Measure() {
        //Empty constructor
    }

    /**
     * Returns a measure with given value and unit
     *
     * @param value Numerical value of the measure
     * @param unit Units of the measure
     */
    public Measure(double value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    /**
     * @return Numerical value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the numerical value of the measure
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * @return the unit of the measure
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the units of the measure
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }
}

package com.magvar.ssapi.dataType;

/**
 * Survey POJO
 */
public class Survey {

    private String trackingId;
    private Measure measuredDepth;
    private String surveyDate;
    private String type;
    private Measure inclination;
    private Measure azimuth;
    private Measure bx;
    private Measure by;
    private Measure bz;
    private Measure gx;
    private Measure gy;
    private Measure gz;

    /**
     * @return the trackingId
     */
    public String getTrackingId() {
        return trackingId;
    }

    /**
     * @param trackingId the trackingId to set
     */
    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    /**
     * @return the measuredDepth
     */
    public Measure getMeasuredDepth() {
        return measuredDepth;
    }

    /**
     * @param measuredDepth the measuredDepth to set
     */
    public void setMeasuredDepth(Measure measuredDepth) {
        this.measuredDepth = measuredDepth;
    }

    /**
     * @return the surveyDate
     */
    public String getSurveyDate() {
        return surveyDate;
    }

    /**
     * @param surveyDate the surveyDate to set
     */
    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the inclination
     */
    public Measure getInclination() {
        return inclination;
    }

    /**
     * @param inclination the inclination to set
     */
    public void setInclination(Measure inclination) {
        this.inclination = inclination;
    }

    /**
     * @return the azimuth
     */
    public Measure getAzimuth() {
        return azimuth;
    }

    /**
     * @param azimuth the azimuth to set
     */
    public void setAzimuth(Measure azimuth) {
        this.azimuth = azimuth;
    }

    /**
     * @return the bx
     */
    public Measure getBx() {
        return bx;
    }

    /**
     * @param bx the bx to set
     */
    public void setBx(Measure bx) {
        this.bx = bx;
    }

    /**
     * @return the by
     */
    public Measure getBy() {
        return by;
    }

    /**
     * @param by the by to set
     */
    public void setBy(Measure by) {
        this.by = by;
    }

    /**
     * @return the bz
     */
    public Measure getBz() {
        return bz;
    }

    /**
     * @param bz the bz to set
     */
    public void setBz(Measure bz) {
        this.bz = bz;
    }

    /**
     * @return the gx
     */
    public Measure getGx() {
        return gx;
    }

    /**
     * @param gx the gx to set
     */
    public void setGx(Measure gx) {
        this.gx = gx;
    }

    /**
     * @return the gy
     */
    public Measure getGy() {
        return gy;
    }

    /**
     * @param gy the gy to set
     */
    public void setGy(Measure gy) {
        this.gy = gy;
    }

    /**
     * @return the gz
     */
    public Measure getGz() {
        return gz;
    }

    /**
     * @param gz the gz to set
     */
    public void setGz(Measure gz) {
        this.gz = gz;
    }
}

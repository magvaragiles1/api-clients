package com.magvar.ssapi;

import com.google.gson.Gson;
import com.magvar.ssapi.dataType.Survey;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Api caller.
 *
 * This class contains all the logic to call survey submission api
 *
 */
public class ApiCaller {

    static final String SERVER = "https://test-ssapi.magvar.com"; //Survey Submission API address
    static final String WELLBORES_URL = SERVER + "/v1/api/wellbores"; //fetches the list of wellbores that client has access to
    static final String SURVEYS_BY_WELLBORE_URL = SERVER + "/v1/api/wellbores/"; //returns wellbore information of the supplied wellbore id
    static final String SUBMIT_SURVEYS_URL = SERVER + "/v1/api/wellbores/{wellboreId}/surveys"; //submits survey to the active survey set for a given wellbore id

    /**
     * If you know your base-64 encoded string of username and password use
     * second authentication line (preferred). If you choose to use your
     * username and password uncomment out the 4 lines below and use them
     * instead For more information on HTTP Basic Auth, please refer to the
     * following links:
     * https://docs.oracle.com/javase/8/docs/api/java/util/Base64.Encoder.html
     * https://en.wikipedia.org/wiki/Basic_access_authentication#Client_side
     */
    String username = "username";
    String password = "password";
    //byte[] bytesCredentialsEncoded = Base64.getEncoder().encode((username + ":" + password).getBytes()); // Encode data using BASE64
    //To use base64 add the below line to the import section.
    //import java.util.Base64;
    //String AUTHENTICATION =  "Basic "+new String(bytesCredentialsEncoded);
    static final String AUTHENTICATION = "Basic aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; //replace the base-64 encoding with your credentials
    //Reference wellboreId
    static final String WELLBORE_ID = "590777956219e9633cb69402"; //The wellbore id used for this example is retrieved from getWellbores() call
    //Http client
    OkHttpClient client = new OkHttpClient();

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * This method sends a requests to the server and fetches all the wellbores
     * user has access to.
     *
     * This is the initial call that will fetch all the wellbore ids which is
     * necessary for getSurveys() and postSurvey() calls.
     *
     * @return String Returns all the wellbores that are accessible for the
     * given account
     * @throws IOException If an IO exception occurred while executing the
     * request
     */
    public String getWellbores() throws IOException {
        Request request = new Request.Builder()
                .url(WELLBORES_URL)
                .header("Authorization", AUTHENTICATION)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    /**
     * This method sends a requests to the server and fetches all the surveys in
     * the wellbore for a given wellboreId
     *
     * @param wellboreId id of the wellbore. List of available wellbore ids can
     * be obtained through getWellbores().
     * @return Returns all the surveys in the wellbore for the given wellboreId
     * @throws IOException if an IO exception occurred while executing the
     * request
     */
    public String getSurveys(String wellboreId) throws IOException {
        Request request = new Request.Builder()
                .url(SURVEYS_BY_WELLBORE_URL + wellboreId)
                .header("Authorization", AUTHENTICATION)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    /**
     * Posts survey to the active surveySet of the given wellboreId
     *
     * @param wellboreId id of the wellbore. List of available wellbore ids can
     * be obtained through getWellbores().
     * @param newSurvey survey that needs to be added
     * @return Returns all the surveys in the wellbore for the given wellboreId
     * along with the newly added survey
     * @throws IOException If an IO exception occurred while executing the
     * request
     */
    public String postSurvey(String wellboreId, Survey newSurvey) throws IOException {
        Gson gson = new Gson();
        String newSurveyJson = gson.toJson(newSurvey);
        RequestBody body = RequestBody.create(JSON, newSurveyJson);
        String url = SUBMIT_SURVEYS_URL.replace("{wellboreId}", wellboreId);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", AUTHENTICATION)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}

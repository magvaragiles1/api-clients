package com.magvar.ssapi;

import com.magvar.ssapi.dataType.Measure;
import com.magvar.ssapi.dataType.Survey;
import java.io.IOException;
import java.util.Scanner;

public class Driver {

    /**
     * Entry point to the program
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ApiCaller apiCaller = new ApiCaller();
        System.out.println("Welcome to Saphira Survey Submission API. Please select an operation below:\n");
        System.out.println("Press 1: Get list of wellbores" + "\n"
                + "Press 2: Get list of surveys for a sample well" + "\n"
                + "Press 3: Posts a survey to the sample wellbore");
        Scanner sc = new Scanner(System.in);
        String choice = sc.nextLine();
        switch (choice) {
            case "1":
                System.out.println("You have selected to get all the wellbores");
                try {
                    System.out.println(apiCaller.getWellbores());
                } catch (IOException ex) {
                    System.out.println("An error has occurred while getting the wellbore: " + ex);
                }
                break;
            case "2":
                System.out.println("Youu have selected to get the list of surveys");
                try {
                    System.out.println(apiCaller.getSurveys(ApiCaller.WELLBORE_ID));
                } catch (IOException ex) {
                    System.out.println("An error has occurred while getting the surveys: " + ex);
                }
                break;
            case "3":
                System.out.println("You have selected to post a survey");
                try {
                    System.out.println(apiCaller.postSurvey(ApiCaller.WELLBORE_ID, new Driver().getSampleSurvey()));
                } catch (IOException ex) {
                    System.out.println("An error has occurred while posting the survey to the wellbore: " + ex);
                }
                break;
            default:
                System.out.println("Invalid choice. Please enter 1, 2 or 3");
        }
    }

    /**
     * Helper function to build a sample survey object
     *
     * @return a constructed sample survey object
     */
    private Survey getSampleSurvey() {
        Survey newSurvey = new Survey();
        newSurvey.setTrackingId("123-4abc#d");
        newSurvey.setMeasuredDepth(new Measure(1452.3, "METER"));
        newSurvey.setSurveyDate("2017-12-21T10:07:43-07:00");
        newSurvey.setType("STANDARD");
        newSurvey.setInclination(new Measure(12.5, "DEGREE"));
        newSurvey.setAzimuth(new Measure(12.5, "DEGREE"));
        newSurvey.setBx(new Measure(32512, "NANOTESLA"));
        newSurvey.setBy(new Measure(32512, "NANOTESLA"));
        newSurvey.setBz(new Measure(32512, "NANOTESLA"));
        newSurvey.setGx(new Measure(9.134, "M_PER_SEC_SQ"));
        newSurvey.setGy(new Measure(9.134, "M_PER_SEC_SQ"));
        newSurvey.setGz(new Measure(9.134, "M_PER_SEC_SQ"));
        return newSurvey;
    }
}

Saphira Survey Submission API Java Sample Client

Requirements:

Java 8 or newer version

This project is built using Gradle so it can run on a command line and most IDEs. 

To run the project, open the project folder in command line.

1. To build the project
	'./gradlew clean build' for linux
	'gradlew clean build' for Windows

2. To run the project
	'java -jar build/libs/Java.jar'
	
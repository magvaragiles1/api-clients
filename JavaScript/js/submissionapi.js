//Endpoints
var SERVER = 'https://test-ssapi.magvar.com'; //Survey Submission API address
var WELLBORES_URL = SERVER + '/v1/api/wellbores'; //fetches the list of wellbores that client has access to 
var SURVEYS_BY_WELLBORE_URL = SERVER + '/v1/api/wellbores/'; //returns wellbore information of the supplied wellbore id
var SUBMIT_SURVEY_URL = SERVER + '/v1/api/wellbores/{wellboreId}/surveys'; //submits survey to the active survey set for a given wellbore id 

/** 
 * Authentication
 *
 * If you know your base-64 encoded string of username and password use second authentication line (preferred).
 * If you choose to use your username password uncomment out the 3 lines below and use them instead
 * For more information on HTTP Basic Auth, please refer to: 
 * https://en.wikipedia.org/wiki/Basic_access_authentication#Client_side
 */
//var username = 'username';
//var password = 'password';
//var AUTHENTICATION = 'Basic '+btoa(username + ":" + password);
var AUTHENTICATION = 'Basic aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'; //replace the base-64 encoding with your credentials

//Reference wellboreId
var WELLBORE_ID= '5a2c1872bff8561ad82c0021'; //The wellbore id used for this example is retrieved from getWellbores() call

//Sample survey data
var newSurvey = {
    "trackingId": "123-4abc#d",
    "measuredDepth": {
        "value": 1452.3,
        "unit": "METER"
    },
    "surveyDate": "2017-12-21T10:07:43-07:00",
    "type": "STANDARD",
    "inclination": {
        "value": 12.5,
        "unit": "DEGREE"
    },
    "azimuth": {
        "value": 12.5,
        "unit": "DEGREE"
    },
    "bx": {
        "value": 32512,
        "unit": "NANOTESLA"
    },
    "by": {
        "value": 32512,
        "unit": "NANOTESLA"
    },
    "bz": {
        "value": 32512,
        "unit": "NANOTESLA"
    },
    "gx": {
        "value": 9.134,
        "unit": "M_PER_SEC_SQ"
    },
    "gy": {
        "value": 9.134,
        "unit": "M_PER_SEC_SQ"
    },
    "gz": {
        "value": 9.134,
        "unit": "M_PER_SEC_SQ"
    }
};


/**
 * Returns all the wellbores that are accessible
 * for the given account.
 * This is the initial call that will fetch all the wellbore ids 
 * which is necessary for getSurveys() and postSurvey() calls.
 */
function getWellbores() {
    var xmlHttpRequest = prepareRequest(WELLBORES_URL, 'GET');
    xmlHttpRequest.onload = function() {
        var data = xmlHttpRequest.responseText;
        if (xmlHttpRequest.status != 200) {
            alert(data);
        } else {
            setValue(data);
        }
    }
    xmlHttpRequest.send();
}

/**
 * Returns all the surveys in the wellbore for the given wellboreId.
 * List of available wellbore ids can be obtained through getWellbores().
 */
function getSurveys(wellboreId) {
    var url = SURVEYS_BY_WELLBORE_URL + wellboreId;
    var xmlHttpRequest = prepareRequest(url, 'GET');
    xmlHttpRequest.onload = function() {
        var data = xmlHttpRequest.responseText;
        if (xmlHttpRequest.status != 200) {
            alert(data);
        } else {
            setValue(data);
        }
    }
    xmlHttpRequest.send();
}

/**
 * Posts survey to the active surveyset of the given wellboreId
 * List of available wellbore ids can be obtained through getWellbores().
 */
function postSurvey(wellboreId) {
    var url = SUBMIT_SURVEY_URL.replace('{wellboreId}', wellboreId);
    var xmlHttpRequest = prepareRequest(url, 'POST');
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json");
    xmlHttpRequest.onload = function() {
        var data = xmlHttpRequest.responseText;
        if (xmlHttpRequest.status != 201) {
            alert(data);
        } else {
            setValue(data);
        }
    }
    xmlHttpRequest.send(JSON.stringify(newSurvey));
}

/**
 * Creates the request object and adds common headers
 */
function prepareRequest(url, method) {
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open(method, url, true);
    xmlHttpRequest.setRequestHeader('Authorization', AUTHENTICATION);
    return xmlHttpRequest;
}

/**
 * Sets values to the HTML element
 */
function setValue(data) {
    document.getElementById('data').innerHTML = data;
}

/**
 * Clears the data in the HTML element
 */
function clearData() {
    document.getElementById('data').innerHTML = "";
}

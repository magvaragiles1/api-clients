Saphira Survey Submission API JavaScript Sample Client

Requirements:
Apache server

To run this client, point the apache DocumentRoot to the index.html (Make sure js folder is next to index.html). 
Once the configuration is done, start/restart apache server. 
Navigate to http://localhost in a web browser.

This will open an interactive web page. 

Now your JavaScript client is up and running!

References:
Installing apache on linux:
http://httpd.apache.org/docs/2.4/install.html

Installing apache on windows:
https://httpd.apache.org/docs/2.4/platform/windows.html

Changing DocumentRoot
https://www.digitalocean.com/community/tutorials/how-to-move-an-apache-web-root-to-a-new-location-on-ubuntu-16-04